## New documentation site for ParaViewCatalyst

All documentation resources regarding ParaViewCatalyst along with a getting started guide are
now part of the ParaView documentation website.  See the new section
[here](https://docs.paraview.org/en/latest/Catalyst/).

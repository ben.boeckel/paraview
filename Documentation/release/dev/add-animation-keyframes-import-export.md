## Add import and export button for animations

Add the possibility to export / import animations into a .pvkfc file format in json.
This format cover all keyframes types (property animation and cameras).

![New interpolate camera widget](add-animation-keyframes-import-export.png)

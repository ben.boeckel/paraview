## Add multiline expression editor in Python calculator

The Python calculator now supports using mutliline expressions. The result array is returned using `return` in the multiline statement. The multiline expression supports autocomplete and Python syntax highlighting.
